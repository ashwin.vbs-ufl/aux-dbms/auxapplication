function onPageLoad(){
    registerEventHandlers();
    DataStore.createTables();

    REST.get("account/status", {}).then(
        function(result){
            if(result){
                GlobalUI.refreshLibrary();
                GlobalUI.getMetrics();
                $$("mainScreen").show();
            }
        }
    );
}

var libView = false;

GlobalUI = {
    getMetrics: function(){
        REST.post("metric/region", {})
        .then(DataStore.fetchSongsForSongInstances)
        .then(DataStore.fetchPerformancesForSongInstances)
        .then(DataStore.fetchArtistsForSongInstances)
        .then(DataStore.fleshOutSongMetrics)
        .then(function(data){
            $$("regionMetric").clearAll();
            $$("regionMetric").parse(data);
        });

        var date00 = new Date(2000, 1, 1);
        var date10 = new Date(2010, 1, 1);
        var date16 = new Date(2016, 1, 1);

        REST.post("metric/period", {
            minTime: date00.getTime()/1000,
            maxTime: date10.getTime()/1000
        })
        .then(DataStore.fetchSongsForSongInstances)
        .then(DataStore.fetchPerformancesForSongInstances)
        .then(DataStore.fetchArtistsForSongInstances)
        .then(DataStore.fleshOutSongMetrics)
        .then(function(data){
            $$("00sMetric").clearAll();
            $$("00sMetric").parse(data);
        });

        REST.post("metric/period", {
            minTime: date10.getTime()/1000,
            maxTime: date16.getTime()/1000
        })
        .then(DataStore.fetchSongsForSongInstances)
        .then(DataStore.fetchPerformancesForSongInstances)
        .then(DataStore.fetchArtistsForSongInstances)
        .then(DataStore.fleshOutSongMetrics)
        .then(function(data){
            $$("10sMetric").clearAll();
            $$("10sMetric").parse(data);
        });
    },

    updateAndShowAlbumView: function(ALBUMID){
        var p1 = new Promise(function(resolve){
            list = alasql("select * from album where ALBUMID = " + ALBUMID);
            resolve(list);
        })
        .then(DataStore.prepAlbums)
        .then(function(records){
            $$("albumPageTitle").parse(records[0]);
            return records;
        }).then(DataStore.fetchSongsFromAlbums)
        .then(function(records){
            if(libView){
                return REST.get("library", {})
                        .then(function(results){
                            return alasql("select * from songinstance where ALBUMID = " + ALBUMID + " and SONGID in (select SONGID from ? where ALBUMID = " + ALBUMID + ")  order by TRACKNUMBER", [results]);
                        });
            } else {
                return alasql("select * from songinstance where ALBUMID = " + ALBUMID + " order by TRACKNUMBER");
            }
        })
        .then(DataStore.fetchSongsForSongInstances)
        .then(DataStore.fetchPerformancesForSongInstances)
        .then(DataStore.fetchArtistsForSongInstances)
        .then(DataStore.prepSongInstances)
        .then(function(result){
            if(libView){
                result.forEach(function(row){
                    row.SUBSCRIBE = "Rem";
                });
            }
            $$("albumPageSongs").clearAll();
            $$("albumPageSongs").parse(result);
            $$("albumPage").show();
        });
    },

    updateAndShowArtistView: function(ARTISTID){
        var p1 = new Promise(function(resolve){
            list = alasql("select * from artist where ARTISTID = " + ARTISTID);
            resolve(list);
        })
        .then(DataStore.prepArtists)
        .then(function(records){
            $$("artistPageTitle").parse(records[0]);
            return records;
        }).then(DataStore.fetchAlbumsForArtists)
        .then(function(records){
            if(libView){
                return REST.get("library", {})
                        .then(function(results){
                            return alasql("select * from album where ARTISTID = " + ARTISTID + " and ALBUMID in (select ALBUMID from ?)", [results]);
                        });
            } else {
                return alasql("select * from album where ARTISTID = " + ARTISTID);
            }
        })
        .then(DataStore.prepAlbums)
        .then(function(result){
            $$("artistPageAlbums").clearAll();
            $$("artistPageAlbums").parse(result);
            $$("artistPage").show();
        });
    },

    playSong: function(ID){

    },

    addSIToLibrary: function(albumid, songid){
        REST.put("library", {
            ALBUMID: albumid,
            SONGID: songid
        }).then(function(){
            alasql("insert into libarary values (" + albumid + ", " + artistid + ")");
        });
    },

    delSIFromLibrary: function(albumid, songid){
        REST.delete("library", {
            ALBUMID: albumid,
            SONGID: songid
        }).then(function(){
            alasql("delete from libarary where ALBUMID = " + albumid + " and ARTISTID = " + artistid + ")");
        });
    },

    refreshLibrary: function(){
        REST.get("library", {})
            .then(DataStore.storeSongInstances)
            .then(function(instances){
                return alasql("select * from songinstance where exists(select * from ? as test where test.ALBUMID = songinstance.ALBUMID and test.SONGID = songinstance.SONGID)", [instances]);
            })
            .then(DataStore.fetchAlbumsForSongInstances)
            .then(DataStore.fetchSongsForSongInstances)
            .then(DataStore.fetchPerformancesForSongInstances)
            .then(DataStore.fetchArtistsForSongInstances)
            .then(DataStore.prepSongInstances)
            .then(function(result){
                result.forEach(function(row){
                    row.SUBSCRIBE = "Rem";
                });
                $$("librarySongTab").clearAll();
                $$("librarySongTab").parse(result);
                return result;
            }).then(function(data){
                albums = alasql("select * from album where ALBUMID in (select ALBUMID from ?)", [data]);
                return albums;
            }).then(DataStore.prepAlbums)
            .then(function(result){
                $$("libraryAlbumTab").clearAll();
                $$("libraryAlbumTab").parse(result);
                return result;
            }).then(function(albums){
                artists = alasql("select * from artist where ARTISTID in (select ARTISTID from ?)", [albums]);
                return artists;
            }).then(DataStore.prepArtists)
            .then(function(result){
                $$("libraryArtistTab").clearAll();
                $$("libraryArtistTab").parse(result);
            });
    }
};

function registerEventHandlers(){

    // login screen functions
    $$("loginScreenRegisterButton").attachEvent("onItemClick", function(){
        $$("createUserScreen").show();
    });

    $$("loginScreenLoginButton").attachEvent("onItemClick", function(){

        REST.post("account/login", {
            USERID: $$("loginScreenUserIDText").getValue(),
            PASSWORD: $$("loginScreenPasswordText").getValue()
        }).then(
            function(result){
                REST.get("account/status", {}).then(
                    function(result){
                        if(result){
                            GlobalUI.refreshLibrary();
                            GlobalUI.getMetrics();
                            $$("mainScreen").show();
                        }
                    }
                );
            }
        );

    });

    $$("loginScreenClearButton").attachEvent("onItemClick", function(){
        $$("loginScreenUserIDText").setValue("");
        $$("loginScreenPasswordText").setValue("");
    });

    // create user screen functions
    $$("createUserScreenBackButton").attachEvent("onItemClick", function(){
        $$("loginScreen").show();
    });

    $$("createUserScreenSubmitButton").attachEvent("onItemClick", function(){
        var date = $$("createUserScreenDate").getValue();
        var unixdate = (date.getTime())/1000;

        REST.post("account/create", {
            USERID: $$("createUserScreenUserID").getValue(),
            NAME: $$("createUserScreenName").getValue(),
            DOB: unixdate,
            PASSWORD: $$("createUserScreenPassword").getValue(),
            REGION: $$("createUserScreenRegion").getValue()
        }).then(
            function(result){
                $$("loginScreen").show();
            }
        );
    });

    //main screen left bar functions
    $$("mainScreenLogoutButton").attachEvent("onItemClick", function(){
        REST.get("account/logout", null).then(
            function(result){
                $$("loginScreen").show();
            }
        );
    });

    $$("navHome").attachEvent("onItemClick", function(){
        $$("metricPage").show();
    });

    $$("navColumn").attachEvent("onItemClick", function(id, e, node){
        libView = false;
        switch(id){
            case "navSearch":
                $$("searchPage").show();
            break;
            case "navLibrary":
                $$("libraryPage").show();
                libView = true;
            break;
            case "navPlaylist":
                $$("playlistPage").show();
            break;
            case "navFriends":
                $$("friendsPage").show();
            break;
            case "navArtist":
                $$("artistPage").show();
            break;
            case "navAlbum":
                $$("albumPage").show();
            break;
        }
    });



    //search screen functions
    $$("searchPageSearch").attachEvent("onSearchIconClick", function(){
        var stage1 = this.getValue().split('"');
        var stage2 = [];
        stage1.forEach(function(currentvalue, index, array){
            if((index+2)%2)
                stage2.concat(currentvalue.split(' '));
            else
                stage2.push(currentvalue);
        });

        $$("searchPageSongResults").clearAll();
        $$("searchPageAlbumResults").clearAll();
        $$("searchPageArtistResults").clearAll();

        REST.post("search/song", {
            SearchTerms: stage2
        })  .then(DataStore.storeSongInstances)
            .then(function(instances){
                return alasql("select * from songinstance where exists(select * from ? as test where test.ALBUMID = songinstance.ALBUMID and test.SONGID = songinstance.SONGID)", [instances]);
            })
            .then(DataStore.fetchAlbumsForSongInstances)
            .then(DataStore.fetchSongsForSongInstances)
            .then(DataStore.fetchPerformancesForSongInstances)
            .then(DataStore.fetchArtistsForSongInstances)
            .then(DataStore.prepSongInstances)
            .then(function(result){
                $$("searchPageSongResults").clearAll();
                $$("searchPageSongResults").parse(result);
            }
        );

        REST.post("search/album", {
            SearchTerms: stage2
        })  .then(function(data){return data;})
            .then(DataStore.storeAlbums)
            .then(DataStore.fetchArtistsForAlbums)
            .then(DataStore.prepAlbums)
            .then(function(result){
                $$("searchPageAlbumResults").clearAll();
                $$("searchPageAlbumResults").parse(result);
            }
        );

        REST.post("search/artist", {
            SearchTerms: stage2
        })  .then(function(data){return data;})
            .then(DataStore.storeArtists)
            .then(DataStore.prepArtists)
            .then(function(result){
                $$("searchPageArtistResults").clearAll();
                $$("searchPageArtistResults").parse(result);
            }
        );
    });

    $$("searchPageSongResults").attachEvent("onItemDblClick", function(id){
        GlobalUI.playSong(id);
    });

    $$("searchPageAlbumResults").attachEvent("onItemDblClick", function(id){
        GlobalUI.updateAndShowAlbumView(id);
    });

    $$("searchPageArtistResults").attachEvent("onItemDblClick", function(id){
        GlobalUI.updateAndShowArtistView(id);
    });

    $$("searchPageSongResults").attachEvent("onItemClick", function(id){
        if(id.column == "SUBSCRIBE"){
            console.log(id);
            DataStore.subscribe(id.row, libView)
                .then(function(){
                    GlobalUI.refreshLibrary();
                });
        }
    });

    //friends page functions
    $$("friendsPageSearch").attachEvent("onSearchIconClick", function(){
        var stage1 = this.getValue().split('"');
        var stage2 = [];
        stage1.forEach(function(currentvalue, index, array){
            if((index+2)%2)
                stage2.concat(currentvalue.split(' '));
            else
                stage2.push(currentvalue);
        });

        $$("friendsPageSearchResults").clearAll();

        REST.post("search/account", {
            SearchTerms: stage2
        })  .then(function(result){
                result.forEach(function(record){
                    record.FRIEND = "Add";
                });
                $$("friendsPageSearchResults").clearAll();
                $$("friendsPageSearchResults").parse(result);
            }
        );
    });

    //library functions
    $$("libraryArtistTab").attachEvent("onItemDblClick", function(id){
        GlobalUI.updateAndShowArtistView(id);
    });
    $$("libraryAlbumTab").attachEvent("onItemDblClick", function(id){
        GlobalUI.updateAndShowAlbumView(id);
    });
    $$("librarySongTab").attachEvent("onItemDblClick", function(id){
        GlobalUI.playSong(id);
    });
    $$("librarySongTab").attachEvent("onItemClick", function(id){
        if(id.column == "SUBSCRIBE"){
            DataStore.subscribe(id.row, libView)
                .then(function(){
                    GlobalUI.refreshLibrary();
                })
                .then(function(){
                    $$("albumPageSongs").remove(id);
                });
        }
    });

    //artistpage functions
    $$("artistPageAlbums").attachEvent("onItemDblClick", function(id){
        GlobalUI.updateAndShowAlbumView(id);
    });

    // albumpage functions
    $$("albumPageSongs").attachEvent("onItemDblClick", function(id){
        GlobalUI.playSong(id);
    });

    $$("albumPageSongs").attachEvent("onItemClick", function(id){
        if(id.column == "SUBSCRIBE"){
            DataStore.subscribe(id.row, libView)
                .then(function(){
                    GlobalUI.refreshLibrary();
                })
                .then(function(){
                    if(libView)
                        $$("albumPageSongs").remove(id);
                });;
        }
    });
}

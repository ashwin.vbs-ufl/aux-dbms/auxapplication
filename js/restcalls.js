var RestRoot = "/~abalasub/dbms/rest/";

REST = {
    call: function(method, path, data){
        return new Promise(function(resolve, reject){
            var xhr = new XMLHttpRequest();
            xhr.open(method, RestRoot + path, true);
            xhr.onreadystatechange = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.responseText).result);
                    } else {
                        reject(Error(xhr.statusText));
                    }
                }
            };
            xhr.onerror = function (e) {
                console.log(Error(xhr.statusText));
            };
            xhr.send(JSON.stringify(data));
        });
    },

    get: function(path, data){
        return this.call("GET", path, data);
    },

    post: function(path, data){
        return this.call("POST", path, data);
    },

    put: function(path, data){
        return this.call("PUT", path, data);
    },

    delete: function(path, data){
        return this.call("DELETE", path, data);
    }

};

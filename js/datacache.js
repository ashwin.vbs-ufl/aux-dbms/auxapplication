DataStore = {
    createTables:function(){
        alasql("create table songinstance(id int unique not null auto_increment, ALBUMID int, SONGID int, TRACKNUMBER int, primary key (ALBUMID, SONGID))");
        alasql("create table song(SONGID int, SONGNAME string, DATEOFPERFORMANCE int, GENRE string, primary key (SONGID))");
        alasql("create table album(ALBUMID int, ALBUMNAME string, ARTISTID int, YEAR int, primary key (ALBUMID))");
        alasql("create table artist(ARTISTID int, ARTISTNAME string, DOB int, primary key (ARTISTID))");
        alasql("create table performance(ARTISTID int, SONGID int, primary key (ARTISTID, SONGID))");
    },

    fleshOutSongMetrics: function(data){
        data.forEach(function(currentValue, index, array){
            song = alasql("select distinct * from song where SONGID = " + currentValue.SONGID);
            artists = alasql("select * from artist where ARTISTID in (select ARTISTID from performance where SONGID = " + currentValue.SONGID + ")");
            currentValue.ARTISTNAME = "";
            for(var i = 0; i < artists.length; i++){
                currentValue.ARTISTNAME = currentValue.ARTISTNAME.concat(artists[i].ARTISTNAME);
                if(i < artists.length-1)
                    currentValue.ARTISTNAME = currentValue.ARTISTNAME.concat(", ");
            }

            currentValue.SONGNAME = song[0].SONGNAME;
        });
        return data;
    },

    storeSongInstances:function(data){
        data.forEach(function(row){
            temp = alasql("select * from songinstance where SONGID = " + row.SONGID + " and ALBUMID = " + row.ALBUMID);
            if(temp.length == 0){
                alasql("insert or replace into songinstance select * from ?", [[row]]);
            }
        });
        return data;
    },

    fetchAlbumsForSongInstances:function(data){
        var albumFetchList = alasql("select distinct ALBUMID from ? where ALBUMID not in (select ALBUMID from album)", [data]);
        var promiselist = [];
        albumFetchList.forEach(function(currentValue, index, array){
            promiselist[index] = REST.get("album/" + currentValue.ALBUMID, {});
        });

        return Promise.all(promiselist).then(function(values){
            albumRecords = [];
            values.forEach(function(value){
                albumRecords.push(value[0]);
            });
            alasql("insert or replace into album select * from ?", [albumRecords]);
            return data;
        });
    },

    fetchSongsForSongInstances:function(data){
        var albumFetchList = alasql("select distinct SONGID from ? where SONGID not in (select SONGID from song)", [data]);
        var promiselist = [];
        albumFetchList.forEach(function(currentValue, index, array){
            promiselist[index] = REST.get("song/" + currentValue.SONGID, {});
        });

        return Promise.all(promiselist).then(function(values){
            songRecords = [];
            values.forEach(function(value){
                songRecords.push(value[0]);
            });
            alasql("insert or replace into song select * from ?", [songRecords]);
            return data;
        });
    },

    fetchPerformancesForSongInstances:function(data){
        var albumFetchList = alasql("select distinct SONGID from ? where SONGID not in (select SONGID from performance)", [data]);
        var promiselist = [];
        albumFetchList.forEach(function(currentValue, index, array){
            promiselist[index] = REST.get("song/" + currentValue.SONGID + "/artists", {});
        });
        return Promise.all(promiselist).then(function(values){
            performanceRecords = [];
            values.forEach(function(value){
                performanceRecords.push(value[0]);
            });
            alasql("insert or replace into performance select * from ?", [performanceRecords]);
            return data;
        });
    },

    fetchArtistsForSongInstances:function(data){
        var albumFetchList = alasql("select distinct performance.ARTISTID from ? as test, performance where test.SONGID = performance.SONGID and performance.ARTISTID not in (select ARTISTID from artist)", [data]);
        var promiselist = [];
        albumFetchList.forEach(function(currentValue, index, array){
            promiselist[index] = REST.get("artist/" + currentValue.ARTISTID, {});
        });
        return Promise.all(promiselist).then(function(values){
            artistRecords = [];
            values.forEach(function(value){
                artistRecords.push(value[0]);
            });
            alasql("insert or replace into artist select * from ?", [artistRecords]);
            return data;
        });
    },

    prepSongInstances:function(data){
        data.forEach(function(currentValue, index, array){
            album = alasql("select distinct * from album where ALBUMID = " + currentValue.ALBUMID);
            song = alasql("select distinct * from song where SONGID = " + currentValue.SONGID);
            artists = alasql("select * from artist where ARTISTID in (select ARTISTID from performance where SONGID = " + currentValue.SONGID + ")");
            currentValue.ARTISTNAME = "";
            for(var i = 0; i < artists.length; i++){
                currentValue.ARTISTNAME = currentValue.ARTISTNAME.concat(artists[i].ARTISTNAME);
                if(i < artists.length-1)
                    currentValue.ARTISTNAME = currentValue.ARTISTNAME.concat(", ");
            }

            currentValue.SONGNAME = song[0].SONGNAME;
            currentValue.ALBUMNAME = album[0].ALBUMNAME;

            currentValue.SUBSCRIBE = "Add";
        });
        return data;
    },

    storeAlbums: function(data){
        alasql("insert or replace into album select * from ?", [data]);
        return data;
    },

    fetchArtistsForAlbums: function(data){
        var artistList = alasql("select distinct ARTISTID from ? where ARTISTID not in (select ARTISTID from artist)", [data]);
        var promiselist = [];
        artistList.forEach(function(row, index, array){
            promiselist[index] = REST.get("artist/" + row.ARTISTID, {});
        });
        return Promise.all(promiselist).then(function(values){
            artistRecords = [];
            values.forEach(function(value){
                artistRecords.push(value[0]);
            });
            alasql("insert or replace into artist select distinct * from ?", [artistRecords]);
            return data;
        });
    },

    fetchAlbumsForArtists: function(data){
        var promiselist = [];
        data.forEach(function(currentValue, index, array){
            promiselist[index] = REST.get("artist/" + currentValue.ARTISTID + "/albums", {});
        });
        return Promise.all(promiselist).then(function(values){
            albumRecords = [];
            values.forEach(function(value){
                value.forEach(function(record){
                    albumRecords.push(record);
                });
            });
            alasql("insert or replace into album select distinct * from ?", [albumRecords]);
            return data;
        });
    },

    fetchSongsFromAlbums: function(data){
        var promiselist = [];
        data.forEach(function(currentValue, index, array){
            promiselist[index] = REST.get("album/" + currentValue.ALBUMID + "/songs", {});
        });
        return Promise.all(promiselist).then(function(values){
            siRecords = [];
            values.forEach(function(value){
                value.forEach(function(record){
                    siRecords.push(record);
                });
            });
            siRecords.forEach(function(row){
                temp = alasql("select * from songinstance where SONGID = " + row.SONGID + " and ALBUMID = " + row.ALBUMID);
                if(temp.length == 0){
                    alasql("insert or replace into songinstance select * from ?", [[row]]);
                }
            });
            return data;
        });
    },

    prepAlbums: function(data){
        data.forEach(function(currentValue, index, array){
            artist = alasql("select * from artist where ARTISTID = " + currentValue.ARTISTID);
            currentValue.ARTISTNAME = artist[0].ARTISTNAME;

            var d = new Date(currentValue.YEAR*1000);
            currentValue.YEARSTR = d.getFullYear().toString();

            currentValue.id = currentValue.ALBUMID;
        });
        return data;
    },

    storeArtists:function(data){
        alasql("insert or replace into artist select * from ?", [data]);
        return data;
    },

    prepArtists: function(data){
        data.forEach(function(currentValue, index, array){
            var d = new Date(currentValue.DOB*1000);
            currentValue.DOBSTR = d.getFullYear().toString();

            currentValue.id = currentValue.ARTISTID;
        });
        return data;
    },

    prepAccounts: function(data){
        data.forEach(function(currentValue, index, array){
            currentValue.id = currentValue.USERID;
        });
        return data;
    },

    subscribe: function(id, libView){
        result = alasql("select * from songinstance where id = " + id);
        if(!libView){
            return REST.put("library", {
                ALBUMID: result[0].ALBUMID,
                SONGID: result[0].SONGID
            }).then(function(test){});
        } else {
            return REST.delete("library", {
                ALBUMID: result[0].ALBUMID,
                SONGID: result[0].SONGID
            }).then(function(test){});
        }
    }

};
